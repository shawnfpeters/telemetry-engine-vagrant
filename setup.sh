yum update

# Install Git
yum install -y git
mkdir /home/vagrant/projects
cd /home/vagrant/projects

# Configure SSH to BitBucket for root (install)
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts
ssh -T git@bitbucket.org

# Clone telemetry-engine
git clone git@bitbucket.org:dragosinc/telemetry-engine.git

# Install Telemetry Engine helpers
cd telemetry-engine/provision-engine
./install-helpers.sh
export VAULT_ADDR=https://vault.dragos.com
export VAULT_SKIP_VERIFY=1

# Clone airflow
# cd /home/vagrant/projects
# git clone git@bitbucket.org:dragosinc/telemetry-airflow-dags.git

# Install Dags
# cd /home/vagrant/projects/telemetry-airflow-dags
# ./installer.sh


# Install Docker
# yum install -y yum-utils
# yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
# yum install -y docker-ce docker-ce-cli containerd.io

# Start Docker
# systemctl start docker